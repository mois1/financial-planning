package cz.uhk.mois.financialplanning.service.planning;

/**
 * This is the type of savings according to which the financial (/ savings) plan will be created.
 *
 * @author Jan Krunčík
 * @since 11.04.2020 15:14
 */

public enum SavingsType {

    MONTHLY,
    ANNUAL
}
