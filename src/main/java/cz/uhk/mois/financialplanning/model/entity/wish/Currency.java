package cz.uhk.mois.financialplanning.model.entity.wish;

/**
 * @author Jan Krunčík
 * @since 22.03.2020 15:26
 */

public enum Currency {

    CZK
}
