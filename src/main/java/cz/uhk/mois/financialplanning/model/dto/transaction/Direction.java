package cz.uhk.mois.financialplanning.model.dto.transaction;

/**
 * @author Jan Krunčík
 * @since 03.04.2020 1:55
 */

public enum Direction {

    INCOMING, OUTGOING, BOTH
}
